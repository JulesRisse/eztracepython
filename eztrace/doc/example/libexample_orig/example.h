/* -*- c-file-style: "GNU" -*- */
/*
 * Copyright (C) CNRS, INRIA, Université Bordeaux 1, Télécom SudParis
 * See COPYING in top-level directory.
 */

#ifndef __EXAMPLE_H__
#define __EXAMPLE_H__

/* dummy function #1 */
double example_function1(double*, int);

/* dummy function #2 */
int example_function2(int*, int);

double example_static_function(double *array, int array_size);
#endif	/* __EXAMPLE_H__ */
