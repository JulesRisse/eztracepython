set(dep_message "\nConfiguration of EZtrace:\n"
  "	General:\n"
  "		Install directory : ${CMAKE_INSTALL_PREFIX}\n"
  "		Compiler: C       : ${CMAKE_C_COMPILER} (${CMAKE_C_COMPILER_ID})\n"
  "		Compiler: Fortran : ${CMAKE_Fortran_COMPILER} (${CMAKE_Fortran_COMPILER_ID})\n"
  "		BFD Path	  : ${BFD_LIBRARY}\n"
  "\n"
  "		Operating system  : ${CMAKE_SYSTEM_NAME}\n")

  set(dep_message "${dep_message}\n		enable binary instrumentation : ")
if (ENABLE_BIN_INSTRUMENTATION)
  set(dep_message "${dep_message}" "Yes\n")
else()
  set(dep_message "${dep_message}" "No\n")
endif()


set(dep_message "${dep_message}\n"
  "  MPI module      : ${EZTRACE_ENABLE_MPI}\n"
  "  CUDA module     : ${EZTRACE_ENABLE_CUDA}\n"
  "  StarPU module   : ${EZTRACE_ENABLE_STARPU}\n"
  "  OpenMP module   : ${EZTRACE_ENABLE_OPENMP}\n"
#  "  PAPI module     : ${EZTRACE_ENABLE_PAPI}\n"
  "  POSIXIO module  : ${EZTRACE_ENABLE_POSIXIO}\n"
  "  Pthread module  : ${EZTRACE_ENABLE_PTHREAD}\n"
  "  Memory module   : ${EZTRACE_ENABLE_MEMORY}\n"
  "  OMPT module     : ${EZTRACE_ENABLE_OMPT}\n"
  "  PNetCDF module  : ${EZTRACE_ENABLE_PNETCDF}\n"
  "  NetCDF module   : ${EZTRACE_ENABLE_NETCDF}\n"
  "  IOtracer module : ${EZTRACE_ENABLE_IOTRACER}\n"
  "  compiler- instrumentation : ${EZTRACE_ENABLE_COMPILER_INSTRUMENTATION}\n"
  "  Python module : ${EZTRACE_ENABLE_PYTHON}\n")

message(${dep_message})
