#include <stdio.h>
#include <omp.h>

int main()
{
  #pragma omp parallel for num_threads(50)
  for (int i=0; i<100; i++) {
    printf("Thread %d running iteration %d\n", omp_get_thread_num(), i);
  }

  return 0;
}
