# Copyright (C) CNRS, INRIA, Université Bordeaux 1, Télécom SudParis
# See COPYING in top-level directory.

BITS=@TARGET_BITS@
CORE_DIRECTORY=@abs_top_builddir@/src/core
CORE_SRC_DIRECTORY=@abs_top_srcdir@/src/core
PPTRACE_DIRECTORY=@abs_top_srcdir@/src/pptrace
TEST_DIRECTORY=@abs_top_srcdir@/test/pptrace/automatic

ifeq "@HAVE_LIBBFD@" "1"
TESTS=errors binary tracing opcodes wait_open hijack
endif

DEBUG_LEVEL=0

ifeq "@PPTRACE_BINARY_TYPE@" "1"
	LIBBIN=-lbfd -lopcodes
else
	LIBBIN=-lelf
endif

PPTRACE_DEPS=errors.c\
	    binary.c

ADDITIONAL_DEPS=$(CORE_SRC_DIRECTORY)/ezt_demangle.c

errors_FLAGS=$(LIBBIN)
errors_DEPS=$(PPTRACE_DEPS)

binary_FLAGS=$(LIBBIN)
binary_DEPS=$(PPTRACE_DEPS)

wait_open_FLAGS=$(LIBBIN)
wait_open_DEPS=$(PPTRACE_DEPS)\
	       tracing.c

tracing_FLAGS=$(LIBBIN)
tracing_DEPS=$(PPTRACE_DEPS)\
	     tracing.c

opcodes_FLAGS=$(LIBBIN)
opcodes_DEPS=$(PPTRACE_DEPS)\
	     opcodes.c

hijack_FLAGS=$(LIBBIN)
hijack_DEPS=	$(PPTRACE_DEPS)\
		opcodes.c\
		tracing.c\
		hijack.c\
		memory.c\
		isize.c

TESTCASES_=$(patsubst %.c,%,$(wildcard $(TEST_DIRECTORY)/testcase/*.c))
TESTCASES=$(foreach testcase,$(TESTCASES_),$(foreach b,$(BITS),$(testcase).$(b) $(testcase).$(b).so))

TESTS_CHECK=$(foreach test,$(TESTS),$(test).check)

CFLAGS = -O0 -g -I$(PPTRACE_DIRECTORY) -I$(CORE_DIRECTORY) -I$(CORE_SRC_DIRECTORY) -D__PPTRACE_DEBUG_LEVEL=$(DEBUG_LEVEL)

.PHONY:	check testcase

all:

distdir:

check:	testcase $(TESTS)

testcase:
	@make -C $(TEST_DIRECTORY)/testcase BITS='$(BITS)'

%.check:	%
	@echo -n " *** RUNNING TEST $<... "
	@./$<.test || (echo "FAILED!"; false)
	@echo "OK"

%::	$(TEST_DIRECTORY)/%.c
	@echo CC	$@
	@$(CC) $(CFLAGS) $^ $(foreach d,$($@_DEPS),$(PPTRACE_DIRECTORY)/$d) $(ADDITIONAL_DEPS) -o $@ $($@_FLAGS) 
	@echo "#!/bin/sh" >$@.test
	@echo "cd $$PWD" >>$@.test
	@echo './$@ ${TESTCASES} $$*' >>$@.test
	@echo 'RES=$$?' >>$@.test
	@echo 'cd - >/dev/null' >>$@.test
	@echo 'exit $$RES'>>$@.test
	@chmod +x $@.test

install:

installcheck:

uninstall:

distclean: clean

clean:
	@rm -f $(TESTS) $(foreach d,$(TESTS),$d.test)
	@if [ -d $(TEST_DIRECTORY)/testcase ] ; then make -C $(TEST_DIRECTORY)/testcase BITS='$(BITS)' clean ; fi
