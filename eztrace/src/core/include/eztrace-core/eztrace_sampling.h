#ifndef EZTRACE_SAMPLING_H
#define EZTRACE_SAMPLING_H

#include <sys/time.h>

struct ezt_sampling_callback_instance;

/* The callback shall return 0 if successfull.
 * If successfull, eztrace core will wait interval microseconds before invoking
 * the callback again.
 * If unsuccessfull, eztrace core may invoke the callback at any time
 */
typedef int (*ezt_sampling_callback_t)(struct ezt_sampling_callback_instance* instance);

/* This structure is passed to sampling callbacks. The callback should not modify any field apart
 * from plugin_data.
 */
struct ezt_sampling_callback_instance {
  ezt_sampling_callback_t callback;
  unsigned interval;
  struct timeval last_call;
  void* plugin_data;
};

/* Register a sampling callback
 * \param callback: the sampling callback
 * \param interval: time in microsecond between each call to callback
 *
 * NB: the interval is thread-specific. Interval corresponds to the minimum
 * time difference between two calls (within one thread). Please note that
 * we use a lazy system: the callback is only called if eztrace intercepts a function.
 * Thus, if the application performs a long computing phase, the callback may not be
 * called very often.
 */
void ezt_sampling_register_callback(ezt_sampling_callback_t callback,
                                    unsigned interval);

void ezt_sampling_check_callbacks();

#endif /* EZTRACE_SAMPLING_H */
