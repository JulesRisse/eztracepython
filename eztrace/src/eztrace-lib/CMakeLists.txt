add_library(eztrace-lib SHARED
  eztrace_core.c
  eztrace_otf2.c
  eztrace_internals.c
  eztrace_module.c
  include/eztrace-lib/eztrace.h
  include/eztrace-lib/eztrace_otf2.h
  include/eztrace-lib/eztrace_internals.h
  include/eztrace-lib/eztrace_module.h
  pthread_core.c
)

target_include_directories(eztrace-lib
  PUBLIC
    $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/include>
    $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDE_DIR}>
  PRIVATE
    ${CMAKE_CURRENT_SOURCE_DIR}
)

target_compile_options(eztrace-lib
  PUBLIC
    -D_GNU_SOURCE
  PRIVATE
    -Wall -Wextra
)

target_link_libraries(eztrace-lib
  PRIVATE
    ${OTF2_LIBRARY}
    atomic
  PUBLIC
    eztrace-core
    dl
    PRIVATE
    ${CMAKE_DL_LIBS}
    rt
    m
    )

install(TARGETS eztrace-lib EXPORT EZTraceLibTargets
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
)

install(DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/include/eztrace-lib DESTINATION ${CMAKE_INSTALL_INCLUDEDIR})
