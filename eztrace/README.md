
# What is EZTrace?

EZTrace is a tool that automatically generates execution traces from
HPC (High Performance Computing) programs. It generates execution trace files
that can be interpreted by visualization tools such as ViTE.


## Trace format

Since version 2.0, EZTrace generates OTF2 traces. You can analyze them
with visualization tools like [ViTE](https://gitlab.inria.fr/solverstack/vite) or Vampir. 
Other automatic analysis tools are being developped here: [https://gitlab.com/eztrace/analysis-tools](https://gitlab.com/eztrace/analysis-tools).

## Supported platforms

EZTrace can run on any Linux platform (yes, even on ARM CPUs). It may
work on other kinds of systems (eg. MacOS) but you may find new
bugs. In this case, don't hesitate to [submit an
Issue](https://gitlab.com/eztrace/eztrace/-/issues).

EZTrace ships several plugins for the main parallel programming libraries, including:
- MPI
- OpenMP
- Pthread
- CUDA
- StarPU

A complete list of existing plugins is [available here](doc/plugin.md).

## Need help ?

If you encounter any problem, you can either:
- browse the [Documentation](#Documentation)
- submit an [Issue](https://gitlab.com/eztrace/eztrace/-/issues)

## Documentation

- [Getting / Building EZTrace](doc/building.md)
- [Using EZTrace](doc/using.md)
- [EZTrace plugins](doc/plugin.md)
- [Frequently asked questions](doc/faq.md)
- [Tutorials](https://gitlab.com/eztrace/eztrace-tutorials)
